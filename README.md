## How does it work

Install the dependencies using the command ```pip install -r requirements.txt```  
  
**Then you will have to provide the Twitter API keys in an `.env` file, otherwise it won't work.**    

I am not feeling comfortable with uploading my personal API keys online, the original plan was to bring it with an USB drive and just run the program off it during the interview.  
  
I uploaded a short video file called `demo.mp4`, which you can see how the program actually works with API keys.  

# Notes

* The program can track up to **400** keywords at once, as far as the API limit goes.  
* I have pretty much completed everything except the _unittest_.  