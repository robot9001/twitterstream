from setuptools import setup, find_packages

setup(
    name='TwitterStream',
    version='0.1.0',
    description='Listens to the active tweets via the given keywords.',
    author='MY',
    author_email='muhammedyuce@protonmail.ch',
    packages=find_packages(include=['code']),
    install_requires=[
        'Flask==1.1.1',
        'tweepy==3.8.0',
        'python-dotenv==0.10.3'
    ],
    package_data={'': ['*.json']},

)