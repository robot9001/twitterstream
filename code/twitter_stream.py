import json
from os import getenv
from time import gmtime, strftime
from dotenv import load_dotenv
from requests import get
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy import StreamListener


load_dotenv()
CONFIG = json.load(open('config.json', 'r'))

USER_AGENT = {'User-Agent': CONFIG['user-agent']}
KEYWORDS = CONFIG['keywords']

CONSUMER_KEY = getenv('CONSUMER')
CONSUMER_SECRET = getenv('CONSUMER_SECRET')
ACCESS_TOKEN = getenv('ACCESS_TOKEN')
ACCESS_TOKEN_SECRET = getenv('ACCESS_TOKEN_SECRET')

AUTH = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
AUTH.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)


def c_time():
    """ Returns the current time. """

    return strftime("| %H:%M:%S |", gmtime())


def flask_api(url):
    """ Sends a GET request to the Flask API and returns the API response. """

    respond = get(f'http://localhost:5000/get?url={url}').content
    return (json.loads(respond))['api_response']


def redirection(url):
    """ Follows the URLs in case of any redirects to grab the actual URL,
                      then returns the final URL """

    redirected = get(url, headers=USER_AGENT, allow_redirects=True).url

    return (redirected == url, redirected)


class Tracker(StreamListener):
    """ Twitter Streaming API that processes the latest tweets."""

    def on_data(self, tweet):
        tweet_json = json.loads(tweet)

        url = tweet_json['entities']['urls']

        if url:
            for link in url:
                address = link['expanded_url']
                if 'twitter.com/' not in address:
                    print(f'\n{c_time()}')

                    redirect = redirection(address)
                    redirect_status, redirect_url = redirect[0], redirect[1]

                    if redirect_status:
                        print(f'API result: {flask_api(redirect_url)}')
                        print(f'Original URL: {address}\nRedirected URL: {redirect_url}')
                    else:
                        print(f'API result: {flask_api(address)}')
                        print('Redirect status: False')


    @staticmethod
    def on_error(status_code):
        print(f'Error: {repr(status_code)}')


if __name__ == '__main__':
    print(f'{c_time()} Listening to the stream!\n')
    T_STREAM = Stream(AUTH, Tracker())
    T_STREAM.filter(track=KEYWORDS, is_async=True)
