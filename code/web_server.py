import json
from os import path
from time import time
from random import getrandbits
from flask import Flask, request


APP = Flask(__name__)

LOG_FILENAME = 'log.json'


""" Creates the log file upon starting the program if it doesn't exist. """

if not path.exists(LOG_FILENAME):
    with open(LOG_FILENAME, 'w') as f:
        json.dump({}, f)


def add_log(obj):
    """ Appends the result to the log file with the UNIX timestamp. """

    with open(LOG_FILENAME, 'r') as read:
        content = json.load(read)

    with open(LOG_FILENAME, 'w') as write:
        obj = json.loads(obj)

        content[f'{str(int(time()))}'] = obj
        json.dump(content, write, indent=4)


@APP.route('/get', methods=['GET'])
def get():
    """ GET method that returns True or False with the requested URL parameter. """

    resp = bool(getrandbits(1))
    url = request.values.get('url')

    params = {"api_response": resp, "requested_url": url}
    j_params = json.dumps(params)
    add_log(j_params)

    return j_params



@APP.route('/post', methods=['POST'])
def post():
    """ POST method that takes a json object and returns True or False
                  with the requested URL parameter"""

    resp = bool(getrandbits(1))
    url = request.get_json().get('url')

    params = {"api_response": resp, "requested_url": url}
    j_params = json.dumps(params)
    add_log(j_params)

    return j_params


APP.run()
